## 나누다 키친 Frontend Homepage and Admin Repository

# Lint and Eslint 적용

    - npm run lint
        - 린트 필요한 파일들을 모두 불러온다.
    - npm run lintfix
        - 린트 적용 가능한 파일들한테 적용을 한다.

# Most common start commands

    - npm run dev
    - npm run generate
    - npm run start build
    - npm run start

# Environemt variables

    - development
    - staging
    - production
