export default function (context) {
    // context.$axios.onRequest(config => {
    //     console.log('config', config)
    //     console.log('Making request to ' + config.url)
    // })

    // context.$axios.onError(error => {
    //     const code = parseInt(error.response && error.response.status)
    //     if (code === 403) {
    //         //403 error를 받으면 로그아웃하고 로그인페이지로 리다이렉트함         
    //         context.store.dispatch('logout')
    //         if (context.route.path.includes('admin')) {
    //             context.redirect('/admin/login')
    //         }
    //         else {
    //             context.redirect('/login')
    //         }
    //     }
    // })
    //토큰이 있으면 항상 http 통신 헤더에 넣음
    if (process.client) {
        if (context.nuxtState.state.token) {
            context.$axios.setHeader("x-access-token", context.nuxtState.state.token)
        }
    }
}