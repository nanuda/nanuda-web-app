import Vue from 'vue'
import jwt from 'jsonwebtoken';

export default (context) => {

    Vue.prototype.$checkTokenExpired = (redirectUrl) => {
        if (context.store.getters.isAuthenticated) {
            try {
                jwt.verify(
                    context.store.state.token,
                    'SeCrEtKeYNanudaHash432')
            } catch (err) {
                context.store.dispatch('logout');
                if (redirectUrl) {
                    context.app.router.push({
                        name: 'login',
                        params: { redirected: redirectUrl }
                    });
                }
            }
        }
    }
}





