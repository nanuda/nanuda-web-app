import Vue from 'vue'
export default function ({ app: { $axios } }) {

  Vue.prototype.$sendMsg = ({ phone, msg, subject = '안녕하세요.나누다키친입니다.', adminPhoneNum }) => {
    let params = { PHONE: phone, MSG: msg, TITLE: subject }
    // if (adminPhoneNum) {
    //   params.SENDER_PHONE = adminPhoneNum
    // }
    let thePromise = $axios.$post('/messageAdmin', params)
    return thePromise
  }
}


