import Vue from 'vue'
import VueResource from 'vue-resource'
import Vuelidate from 'vuelidate'
import VueCryptojs from 'vue-cryptojs'
import autoRotate from 'vue-jpeg-auto-rotation';
import VueNumerals from 'vue-numerals';
import VueRx from 'vue-rx'

Vue.use(Vuelidate)
Vue.use(VueCryptojs)
Vue.use(VueResource)
Vue.use(VueNumerals); // default locale is 'en'
Vue.use(VueRx)
Vue.component('autoRotate', autoRotate)
