import Vue from 'vue';
import VueDatepicker from 'vuejs-datepicker';

Vue.use(VueDatepicker);
Vue.component('VueDatepicker', VueDatepicker);
