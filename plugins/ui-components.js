import Vue from 'vue';

import ToggleBtn from '@/components/UI/ToggleBtn';
import ExpandingCard from '@/components/UI/ExpandingCard';

import CardWithTitle from '@/components/Admin/CardWithTitle';
import TableWithTitle from '@/components/Admin/TableWithTitle';
import SMSBtnAndDialog from '@/components/Admin/SMSBtnAndDialog';
import ManagerSelect from '@/components/Admin/ManagerSelect';

import DatePicker from '@/components/Utils/DatePicker';
import VueCountdown from '@chenfengyuan/vue-countdown';

Vue.component('ToggleBtn', ToggleBtn);
Vue.component('CardWithTitle', CardWithTitle);
Vue.component('TableWithTitle', TableWithTitle);
Vue.component('DatePicker', DatePicker);
Vue.component('ExpandingCard', ExpandingCard);
Vue.component('SMSBtnAndDialog', SMSBtnAndDialog);
Vue.component('ManagerSelect', ManagerSelect);
Vue.component(VueCountdown.name, VueCountdown);

// hompage
import TheHeader from '@/components/Navigation/TheHeader';
import EmptyHeader from '@/components/Navigation/EmptyHeader';
import FormGroup from '@/components/UI/FormGroup';
import FormLabel from '@/components/UI/FormLabel';
import FormControl from '@/components/UI/FormControl';
import FormRow from '@/components/UI/FormRow';
import FormMessage from '@/components/UI/FormMessage';
import InputGroup from '@/components/UI/InputGroup';
import InputField from '@/components/UI/InputField';
import nSelect from '@/components/UI/nSelect';
import NSelect from '@/components/UI/Form/NSelect';
import Radio from '@/components/UI/Radio';
import CheckBox from '@/components/UI/CheckBox';
import BtnGroup from '@/components/UI/BtnGroup';
import Button from '@/components/UI/Button';
import NButton from '@/components/UI/button/NButton';
import Ripple from '@/components/UI/Ripple';
import PrevButton from '@/components/UI/Button/PrevButton';
import CloseButton from '@/components/UI/Button/CloseButton';
import HomeButton from '@/components/UI/Button/HomeButton';
import Badge from '@/components/UI/Badge';
import Modal from '@/components/Modal/Modal';
import NCard from '@/components/UI/card/NCard';
import NCardImg from '@/components/UI/card/NCardImg';
import NCardBody from '@/components/UI/card/NCardBody';
import CustomModal from '@/components/Modal/CustomModal';
import GuideModal from '@/components/Modal/GuideModal';

Vue.component('TheHeader', TheHeader);
Vue.component('EmptyHeader', EmptyHeader);
Vue.component('FormGroup', FormGroup);
Vue.component('FormLabel', FormLabel);
Vue.component('FormControl', FormControl);
Vue.component('FormRow', FormRow);
Vue.component('FormMessage', FormMessage);
Vue.component('InputGroup', InputGroup);
Vue.component('InputField', InputField);
Vue.component('nSelect', nSelect);
Vue.component('NSelect', NSelect);
Vue.component('Radio', Radio);
Vue.component('CheckBox', CheckBox);
Vue.component('BtnGroup', BtnGroup);
Vue.component('Button', Button);
Vue.component('NButton', NButton);
Vue.component('Ripple', Ripple);
Vue.component('PrevButton', PrevButton);
Vue.component('CloseButton', CloseButton);
Vue.component('HomeButton', HomeButton);
Vue.component('Badge', Badge);
Vue.component('Modal', Modal);
Vue.component('NCard', NCard);
Vue.component('NCardImg', NCardImg);
Vue.component('NCardBody', NCardBody);
Vue.component('CustomModal', CustomModal);
Vue.component('GuideModal', GuideModal);
