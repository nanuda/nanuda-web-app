
export default ({ app }, inject) => {
    //시간을 빼고 날짜만 리턴함
    inject('getDateOnly', (results) => {
        let res = [];
        results.map(e => {
            const date = e.createdAt.split(" ")[0];
            e.createdAt = date;
            res.push(e);
        });
        return res
    })
}

