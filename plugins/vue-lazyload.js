import Vue from 'vue';
import VueLazyload from 'vue-lazyload';

import error from '@/assets/svg/common/loader.svg';
import loading from '@/assets/svg/common/loader_small.svg';

Vue.use(VueLazyload, {
  preLoad: 1,
  attempt: 1,
  loading: loading,
});
