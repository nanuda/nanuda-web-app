import Vue from 'vue'

import VueDaumMap from "vue-daum-map";
import { VueDaumPostcode } from "vue-daum-postcode"

Vue.component('VueDaumMap', VueDaumMap)
Vue.component('VueDaumPostcode', VueDaumPostcode)