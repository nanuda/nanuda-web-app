import Vue from 'vue';
import VueGtag from 'vue-gtag';

Vue.use(VueGtag, {
  config: { id: 'G-0KL898XG4N' },
  includes: [{ id: 'AW-449955989' }],
});
