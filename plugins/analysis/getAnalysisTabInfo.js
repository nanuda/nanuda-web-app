import Vue from 'vue';
export default function ({ app: { $axios } }) {
  Vue.prototype.$getAnalysisTabInfo = (url, bCode, category) => {
    // console.log('.$getAnalysisTabInfo category', category)
    if (category) {
      return $axios.get(
        `https://platform-analysis.nanudakitchen.com/analysis-tab/${url}`,
        {
          params: {
            bdongCode: bCode,
            baeminCategoryName: category,
          },
        },
      );
    } else {
      return $axios.get(
        `https://platform-analysis.nanudakitchen.com/analysis-tab/${url}?bdongCode=${bCode}`,
      );
    }
  };
}
