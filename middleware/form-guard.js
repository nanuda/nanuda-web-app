export default function (context) {
    //서버사이드와 클라이언트사이드의 context가 다름
    if (process.client) {
        //등록 후 완료화면에서 뒤로가면 메인화면으로 리다이렉트
        if (context.from.path.includes('complete')) {
            context.redirect('/')
        }
    }
}