export default function (context) {
    //로그인 페이지에 로그인상태인 채로 접속하면 메인페이지로 이동
    if (context.store.getters.isAuthenticated) {
        context.redirect('/')
    }


}