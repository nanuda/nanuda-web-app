export default function (context) {
    //인증되지않은 상태로 특정페이지에 접속하면 메인페이지로 리다이렉트
    if (!context.store.getters.isAuthenticated) {
        context.redirect('/')
    }
}