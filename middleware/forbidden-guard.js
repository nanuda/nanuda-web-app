export default function (context) {

    context.$axios.onError(error => {
        const code = parseInt(error.response && error.response.status)
        if (code === 403) {
            //403 error를 받으면 로그아웃하고 로그인페이지로 리다이렉트함
            context.store.dispatch('logout')
            if (context.route.path.includes('admin')) {
                context.redirect("/admin/login", { redirected: context.route.fullPath }
                )
            }
            else {
                context.redirect('/login')
            }
        }
    })
}