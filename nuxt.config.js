const path = require('path');
const dotenv = require('dotenv');
if (process.env.NODE_ENV === 'development') {
  dotenv.config({ path: path.join(__dirname, './.env.development') });
} else if (process.env.NODE_ENV === 'staging') {
  console.log(process.env.NODE_ENV);
  console.log(process.env.B2B_API_URL);
  dotenv.config({ path: path.join(__dirname, './.env.staging') });
} else if (process.env.NODE_ENV === 'prod') {
  dotenv.config({ path: path.join(__dirname, './.env.production') });
} else {
  throw new Error('No process.env configured!');
}
console.log(process.env.baseURL);
console.log(process.env.b2bBaseURL);
export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  env: {
    baseUrl: process.env.baseURL,
    b2bBaseURL: process.env.b2bBaseURL,
  },
  head: {
    script: [
      {
        src: '//developers.kakao.com/sdk/js/kakao.min.js',
      },
    ],
    title: '나누다키친 | 공유주방의 모든 것',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content:
          'width=device-width,  initial-scale=1.0 , maximum-scale=1.0, minimal-ui, user-scalable=no, viewport-fit=cover',
      },
      {
        hid: 'og:description',
        property: 'og:description',
        content:
          '공유주방 창업하기 전에 공간, 상권, 브랜드 비교까지 나누다키친에서 한 번에 준비하세요',
      },
      {
        hid: 'og:title',
        property: 'og:title',
        content: '나누다키친 | 공유주방의 모든 것',
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: '/meta_main.png',
      },
      { name: 'msapplication-TileColor', content: '#ff7a00' },
      {
        name: 'msapplication-TileImage',
        content: '/meta_main.png',
      },
      { name: 'theme-color', content: '#ff7a00' },
      {
        name: 'apple-mobile-web-app-title',
        content: '나누다키친 | 공유주방의 모든 것',
      },
      { name: 'apple-mobile-web-app-capable', content: 'yes' },
      { name: 'apple-mobile-web-app-status-bar-style', content: 'black' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'shortcut icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'apple-touch-icon',
        sizes: '57x57',
        href: '/apple-icon-57x57.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '60x60',
        href: '/apple-icon-60x60.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '72x72',
        href: '/apple-icon-72x72.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '76x76',
        href: '/apple-icon-76x76.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '114x114',
        href: '/apple-icon-114x114.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '120x120',
        href: '/apple-icon-120x120.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '144x144',
        href: '/apple-icon-144x144.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '152x152',
        href: '/apple-icon-152x152.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/apple-icon-180x180.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '192x192',
        href: '/android-icon-192x192.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/favicon-32x32.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '96x96',
        href: '/favicon-96x96.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/favicon-16x16.png',
      },
      {
        rel: 'apple-touch-startup-image',
        href: '/apple-touch-startup-image.png',
      },
      { rel: 'manifest', href: '/manifest.json' },
    ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: 'rgb(255,130,20)' },
  /*
   ** Global CSS
   */
  // css: ["~/assets/css/common.css"],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/vue-gtag' },
    { src: '~plugins/ui-components.js' },
    '~/plugins/axios',
    { src: '~/plugins/kakaoAPI.js', mode: 'client' },
    { src: '~/plugins/utils.js', mode: 'client' },
    '~/plugins/vuelidate.js',
    { src: '~/plugins/aos', mode: 'client' },
    { src: '~/plugins/vue-odometer.js', mode: 'client' },
    { src: '~/plugins/vue-awesome-swiper.js', mode: 'client' },
    { src: '~/plugins/vuejs-datepicker.js', mode: 'client' },
    { src: '~/plugins/vue-lazyload.js', mode: 'client' },
    { src: '~/plugins/vue-stickykit.js', mode: 'client' },
    '~/plugins/vue-scrollto',
    '~/plugins/munja.js',
    '~/plugins/analysis/getAnalysisTabInfo.js',
    '~/plugins/admin/getDateOnly.js',
    { src: '~/plugins/kakaoSDK.js', mode: 'client' },
    // { src: '~/plugins/vue-slider-component.js', mode: 'client' },
    '~/plugins/auth/validTokenCheck.js',
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    '@nuxtjs/moment',
    [
      '@nuxtjs/google-analytics',
      {
        id: 'G-0KL898XG4N',
      },
    ],
    '@nuxtjs/gtm',
    // [
    //   'nuxt-facebook-pixel-module',
    //   {
    //     /* module options */
    //     track: 'PageView',
    //     pixelId: '432700378140556',
    //     autoPageView: true,
    //     disabled: false,
    //   },
    // ],
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/vuetify',
    '@nuxtjs/style-resources',
    'vue-scrollto/nuxt',
    '@nuxtjs/gtm',
    'nuxt-facebook-pixel-module',
    '@nuxtjs/google-analytics',
  ],
  googleAnalytics: {
    id: 'G-0KL898XG4N',
  },
  gtm: {
    id: 'GTM-T9TXG2X',
    enabled: true,
    pageTracking: true,
    autoInit: true,
  },
  // googleAnalytics: {
  //   id: 'UA-185723597',
  // },
  facebook: {
    /* module options */
    track: 'PageView',
    pixelId: '422533925552381',
    autoPageView: true,
    disabled: false,
  },
  styleResources: {
    scss: ['@/assets/styles/vars/*.scss', '@/assets/styles/mixins/*.scss'],
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.baseURL,
  },
  /*
   ** Build configuration
   */
  build: {
    //layout css를 다른 layout 침범하지않게해줌
    splitChunks: {
      layouts: true,
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // 저장시 ESLint 적용하기
      // 추후 적용
      // if (ctx.isDev && ctx.isClient) {
      //   config.module.rules.push({
      //     enforce: 'pre',
      //     test: /\.(js|vue)$/,
      //     loader: 'eslint-loader',
      //     exclude: /(node_modules)/,
      //   });
      // }
    },
  },
  router: {
    middleware: "check-renewal"
  },
};
