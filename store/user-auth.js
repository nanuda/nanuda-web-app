import Cookie from 'js-cookie';


export const actions = {
    //유저 로그인 액션
    authenticateUser(vuexContext, phoneNum) {
        let authUrl = '/userLogin/login'
        return this.$axios
            .$post(authUrl, {
                PHONE: phoneNum,
            })
            .then(result => {
                //데이터를 api에서 받아서 쿠키와 로컬스토리지에 set함              
                this.$axios.defaults.headers.common['x-access-token'] = result.token;
                vuexContext.commit('setUserName', result.user.NAME, { root: true })
                vuexContext.commit('setUserPhoneNum', result.user.PHONE, { root: true })
                vuexContext.commit('setToken', result.token, { root: true });
                vuexContext.commit('setUserNum', result.user.NO, { root: true });
                localStorage.setItem('token', result.token);
                Cookie.set('jwt', result.token);
                localStorage.setItem('userNum', result.user.NO);
                Cookie.set('userNum', result.user.NO);
            })
            .catch(e => console.log(e));
    }
}


