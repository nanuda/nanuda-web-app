import Cookie from 'js-cookie';
import Vue from 'vue'

export const state = () => ({
    userNum: null,
    token: null,
    userName: null,
    userPhoneNum: null,
    theme: 'dark'
})
export const mutations = {
    setToken(state, token) {
        state.token = token;
    },
    clearToken(state) {
        state.token = null;
    },
    setUserNum(state, userNum) {
        state.userNum = userNum
    },
    clearUserNum(state) {
        state.userNum = null;
    },
    setUserName(state, userName) {
        state.userName = userName
    },
    setUserPhoneNum(state, userPhoneNum) {
        state.userPhoneNum = userPhoneNum
    },
    clearUserName(state) {
        state.userName = null;
    },
    clearUserPhoneNum(state) {
        state.userPhoneNum = null;
    },
    setTheme(state, theme) {
        state.theme = theme;
    },
}
export const actions = {
    initAuth(vuexContext, req) {
        let token;
        let userNum;
        let theme;
        //req는 ssr 일때만 값이 있음
        //쿠키에서 토큰과 userNum을 가져옴
        if (req) {
            // console.log('req', req)
            // console.log('initAuth req headers cookie', req.headers.cookie)
            if (!req.headers.cookie) {
                return;
            }
            //쿠키에서 jwt를 가져옴
            const jwtCookie = req.headers.cookie
                .split(';')
                .find(c => c.trim().startsWith('jwt='));
            if (!jwtCookie) {
                return;
            }

            token = jwtCookie.split('=')[1];
            //쿠키에서 userNum을 가져옴
            const userNumCookie = req.headers.cookie
                .split(';')
                .find(c => c.trim().startsWith('userNum='));
            if (!userNumCookie) {
                return;
            }
            userNum = userNumCookie.split('=')[1];
            //쿠키에서 theme을 가져옴
            const themeCookie = req.headers.cookie
                .split(';')
                .find(c => c.trim().startsWith('theme='));
            if (themeCookie) {
                theme = themeCookie.split('=')[1];
            }
        } else {
            //client process 일때
            token = localStorage.getItem('token');
            userNum = localStorage.getItem('userNum');
            theme = localStorage.getItem('theme')
        }
        //리프레쉬하면 http 디폴트 헤드의 토큰이 사라지기 때문에 다시 추가
        this.$axios.defaults.headers.common['x-access-token'] = token;
        if (!token) {
            vuexContext.dispatch('logout');
            return;
        }
        //store에 token,userNum을 set       
        vuexContext.commit('setToken', token, { root: true });
        vuexContext.commit('setUserNum', userNum, { root: true });
        //로컬스토리지나 쿠키에 theme 이없으면 dark를 디폴트로 넣음
        if (!theme) {
            if (process.client) {
                localStorage.setItem('theme', 'dark')
            } else {
                Cookie.set('theme', 'dark');
            }
            theme = 'dark'
        }
        vuexContext.commit('setTheme', theme, { root: true });
        vuexContext.dispatch('getUserInfo')
    },
    logout(vuexContext) {
        vuexContext.commit('clearToken');
        vuexContext.commit('clearUserNum')
        vuexContext.commit('clearUserName');
        vuexContext.commit('clearUserPhoneNum')
        Cookie.remove('jwt');
        Cookie.remove('userNum');
        if (process.client) {
            localStorage.removeItem('token');
            localStorage.removeItem('userNum');
        }
    },
    getUserInfo(vuexContext, req) {
        if (vuexContext.getters.isAuthenticated) {
            this.$axios.$get('/mypage/userInfo')
                .then(res => {
                    vuexContext.commit('setUserName', res.NAME, { root: true })
                    vuexContext.commit('setUserPhoneNum', res.PHONE, { root: true })
                }).catch(error => {
                    console.log(error)
                })
        }
    },
    setTheme(vuexContext, theme) {
        vuexContext.commit('setTheme', theme, { root: true })
    }
}

export const getters = {
    isAuthenticated(state) {
        return state.token != null;
    }
}



