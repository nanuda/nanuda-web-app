export const state = () => ({
  BRAND: {
    RESTAURANT: null,
    DELIVERY: null,
  },
});
export const mutations = {
  SET_BRAND_RESTAURANT(state, brand) {
    state.BRAND.RESTAURANT = brand;
  },
  SET_BRAND_DELIVERY(state, brand) {
    state.BRAND.DELIVERY = brand;
  },
  // 초기화
  initShowingBrand(state) {
    state.showingBrandsNum = 6;
  },
  showMore(state) {
    // 더보기
    state.showingBrandsNum += 6;
  },
};

export const actions = {
  FETCH_BRAND_RESTAURANT(vuexContext) {
    return this.$axios
      .get('/brand/spaceType/1')
      .then((res) => {
        vuexContext.commit('SET_BRAND_RESTAURANT', res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  },
  FETCH_BRAND_DELIVERY(vuexContext) {
    return this.$axios
      .get('/brand/spaceType/2?limit=12&skip=0')
      .then((res) => {
        vuexContext.commit('SET_BRAND_DELIVERY', res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  },
};
export const getters = {
  showShowMoreBtn(state) {
    if (state.brands) {
      return !(state.showingBrandsNum >= state.brands.length);
    }
  },
};
