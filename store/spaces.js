export const state = () => ({
  spaces: null,
  showingSpacesNum: 15,
  sortingWay: '정렬',
  selectedSpace: null,
});
export const mutations = {
  initSortingWay(state) {
    state.sortingWay = '정렬';
  },
  setSpaces(state, spaces) {
    state.spaces = spaces;
  },
  clearSpaces(state) {
    state.spaces = null;
  },
  //보여주는 공간 수 초기화
  initShowingSpace(state) {
    state.showingSpacesNum = 15;
  },
  showMore(state) {
    //보여주는 공간 수 15개 더보여줌
    state.showingSpacesNum += 15;
  },
  sortHighSizeTop(state) {
    state.sortingWay = '매장면적 넓은 순';
    state.spaces.sort(function (a, b) {
      if (+a.SIZE > +b.SIZE) {
        return -1;
      }
      if (+a.SIZE < +b.SIZE) {
        return 1;
      }
      // a must be equal to b
      return 0;
    });
  },
  sortLowSizeTop(state) {
    state.sortingWay = '매장면적 좁은 순';
    state.spaces.sort(function (a, b) {
      if (+a.SIZE > +b.SIZE) {
        return 1;
      }
      if (+a.SIZE < +b.SIZE) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
  },
  sortHighRentalTop(state) {
    state.sortingWay = '임대료 높은 순';
    state.spaces.sort(function (a, b) {
      if (+a.RENTAL_HOPE_FEE > +b.RENTAL_HOPE_FEE) {
        return -1;
      }
      if (+a.RENTAL_HOPE_FEE < +b.RENTAL_HOPE_FEE) {
        return 1;
      }
      // a must be equal to b
      return 0;
    });
  },
  sortLowRentalTop(state) {
    state.sortingWay = '임대료 낮은 순';
    state.spaces.sort(function (a, b) {
      if (+a.RENTAL_HOPE_FEE > +b.RENTAL_HOPE_FEE) {
        return 1;
      }
      if (+a.RENTAL_HOPE_FEE < +b.RENTAL_HOPE_FEE) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
  },
  sortHighScoreTop(state) {
    state.sortingWay = '추천 높은 순';
    state.spaces.sort(function (a, b) {
      if (+a.SPACE_SCORE > +b.SPACE_SCORE) {
        return -1;
      }
      if (+a.SPACE_SCORE < +b.SPACE_SCORE) {
        return 1;
      }
      // a must be equal to b
      return 0;
    });
  },
  selectSpace(state, space) {
    state.selectedSpace = space;
  },
};
export const actions = {
  getSpaces(vuexContext, params) {
    vuexContext.commit('initShowingSpace');
    vuexContext.commit('initSortingWay');
    //params가 있을때
    if (params) {
      params.CODE = params.CODE === 'all' ? '' : params.CODE || '';
      params.SPACE_TYPE_NO = params.SPACE_TYPE_NO || '';

      return this.$axios
        .$get('/space', {
          params,
        })
        .then((res) => {
          vuexContext.commit('setSpaces', res.SPACE);
        })
        .catch((e) => {
          console.log(e);
        });
      //params가 없을때
    } else {
      return this.$axios
        .$get('/space')
        .then((res) => {
          vuexContext.commit('setSpaces', res.SPACE);
        })
        .catch((e) => {
          console.log(e);
        });
    }
  },
  showMoreSpaces(vuexContext) {
    vuexContext.commit('showMore');
  },
  toHighSizeTop(vuexContext) {
    vuexContext.commit('sortHighSizeTop');
  },
  toLowSizeTop(vuexContext) {
    vuexContext.commit('sortLowSizeTop');
  },
  toHighRentalTop(vuexContext) {
    vuexContext.commit('sortHighRentalTop');
  },
  toLowRentalTop(vuexContext) {
    vuexContext.commit('sortLowRentalTop');
  },
  toHighScoreTop(vuexContext) {
    vuexContext.commit('sortHighScoreTop');
  },
  setSelectedSpace(vuexContext, space) {
    vuexContext.commit('selectSpace', space);
  },
};
export const getters = {
  showShowMoreBtn(state) {
    if (state.spaces) {
      return !(state.showingSpacesNum >= state.spaces.length);
    }
  },
};
