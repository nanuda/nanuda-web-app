import Cookie from 'js-cookie';


export const actions = {
    authenticateAdminUser(vuexContext, authData) {
        let authUrl = '/adminLogin'
        return this.$axios
            .$post(authUrl, {
                PHONE: authData.phoneNumber,
                PASSWORD: authData.password
            })
            .then(result => {
                //로그인 이후 모든 http 헤더에 토근을 추가함
                this.$axios.defaults.headers.common['x-access-token'] = result.token;
                vuexContext.commit('setToken', result.token, { root: true });
                vuexContext.commit('setUserNum', result.user.NO, { root: true });
                localStorage.setItem('token', result.token);
                Cookie.set('jwt', result.token);
                localStorage.setItem('userNum', result.user.NO);
                Cookie.set('userNum', result.user.NO);
            })
            .catch(e => console.log(e));
    },

}



