import FileUpload from 'vue-upload-component';

export const imageUpload = {
    components: {
        FileUpload
    },
    props: {
        category: String,
        uid: Number,
        forAdd: {
            default: false,
            type: Boolean
        },
        totalImagesSize: Number,
        maxSize: Number,
    },
    data() {
        return {
            files: [],
            uploadAuto: !this.forAdd,
            //nuxt.config.js의 env 프로퍼티
            baseUrl: process.env.baseUrl,
            //한번에 올리는 파일들 합친 사이즈
            localChunk: 0
        }
    },
    methods: {
        //추가하기에서 이미지를 올리면 form데이터와 동기화됨
        syncFiles() {
            const key = this.category,
                obj = {
                    [key]: this.files
                };
            this.$emit('syncFiles', obj);

        },
        //버튼 클릭하면 localChunk를 0으로 만듬
        clearLocalChunk() {
            this.localChunk = 0
        },
        //파일을 올리면 emit되는 메소드
        inputFilter(newFile, oldFile, prevent) {
            if (newFile && !oldFile) {
                // Add file
                if (this.forAdd) {
                    this.syncFiles();
                    this.localChunk += newFile.size
                }
                // Filter non-image file
                // Will not be added to files 20971520
                if (!/\.(jpeg|jpe|jpg|gif|png|webp)$/i.test(newFile.name)) {
                    return prevent();
                }
                if (this.localChunk + this.totalImagesSize > this.maxSize) {
                    return prevent();
                }
                if (this.localChunk > this.maxSize) {
                    return prevent();
                }

                // Create the 'blob' field for thumbnail preview
                newFile.blob = '';
                let URL = window.URL || window.webkitURL;
                if (URL && URL.createObjectURL) {
                    newFile.blob = URL.createObjectURL(newFile.file);
                }
            }
            if (newFile && oldFile) {
                // Update file         
                // Increase the version number
                if (!newFile.version) {
                    newFile.version = 0;
                }
                newFile.version++;
            }

            if (!newFile && oldFile) {
                // Remove file
                // Refused to remove the file
                // return prevent()
            }
        },
        //올라간 파일의 이벤트 관리 메소드
        inputFile(newFile, oldFile) {
            //추가하기면 추가한 파일을 부모컴포넌트의 데이터와 싱크함
            if (this.forAdd) {
                this.syncFiles();
            }
            if (newFile && oldFile) {
                // update             
                if (newFile.active && !oldFile.active) {
                    // beforeSend                
                    // min size
                    if (
                        newFile.size >= 0 &&
                        this.minSize > 0 &&
                        newFile.size < this.minSize
                    ) {
                        this.$refs[this.category].update(newFile, { error: 'size' });
                    }
                }
                if (newFile.progress !== oldFile.progress) {
                    // progress
                }
                if (newFile.error && !oldFile.error) {
                    // error
                }
                if (newFile.success && !oldFile.success) {
                    // success                 
                    this.$emit('updateImages', newFile.response[this.category]);
                }
            }
            if (!newFile && oldFile) {
                // remove
                if (oldFile.success && oldFile.response.id) {
                    // $.ajax({
                    //   type: 'DELETE',
                    //   url: '/upload/delete?id=' + oldFile.response.id,
                    // })
                }
            }
            // Automatically activate upload
            if (
                Boolean(newFile) !== Boolean(oldFile) ||
                oldFile.error !== newFile.error
            ) {
                if (this.uploadAuto && !this.$refs[this.category].active) {
                    this.$refs[this.category].active = true;
                }
            }
        },
        color(file) {
            if (file.error) {
                return 'red';
            } else if (file.success) {
                return 'green';
            } else {
                return 'light-blue';
            }
        },
    }
}