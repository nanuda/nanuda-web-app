import AdminNav from '@/components/Admin/AdminNav';
import Cookie from 'js-cookie';
export const adminLayout = {
  middleware: ['check-auth'],
  components: {
    AdminNav,
  },
  props: {
    source: String,
  },
  data: () => ({
    drawer: null,
    theUser: null,
  }),
  computed: {
    theUserName() {
      if (this.theUser) {
        return this.theUser.NAME;
      }
    },
  },
  methods: {
    logout() {
      this.$store.dispatch('logout').then(() => {
        this.$router.push('/admin/login');
      });
    },
    changeTheme() {
      // 테마 쿠키,로컬스토리지
      if (this.$store.state.theme == 'light') {
        this.$store.dispatch('setTheme', 'dark')
        Cookie.set('theme', 'dark');
        localStorage.setItem('theme', 'dark')
      } else {
        this.$store.dispatch('setTheme', 'light')
        Cookie.set('theme', 'light');
        localStorage.setItem('theme', 'light')
      }
    },
  },
  created() {
    if (this.$store.state.theme === 'dark') {
      this.$vuetify.theme.isDark = true;
    } else {
      this.$vuetify.theme.isDark = false
    }
    //header에 스토어에 있는 토큰 추가 없으면 403
    this.$axios.defaults.headers.common[
      'x-access-token'
    ] = this.$store.state.token;
    //스토어에있는 userNum으로 api 찔러서 유저정보 불러와서 data에 넣음
    return this.$axios
      .$get(`/userManagerAdmin/${this.$store.state.userNum}`)
      .then(res => {
        this.theUser = res;
      })
      .catch(e => {
        console.log(e);
      });
  },
};
