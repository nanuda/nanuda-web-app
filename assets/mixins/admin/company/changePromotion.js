export const changePromotion = {
    data() {
        return {
            property: [],
        }
    },
    methods: {
        close() {
            this.$emit('update:show', false);
        },
        addEmptyRow() {
            console.log('add');
            this.property.push({ KEY: '', VALUE: '' });
        },
        deleteRow(index) {
            this.property.splice(index, 1);
        },
    },

}