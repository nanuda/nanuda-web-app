export const changeManager = {
    data() {
        return {
            manager: null
        }
    },
    methods: {
        changeManager(value) {
            this.manager = value;
        },
    }
}