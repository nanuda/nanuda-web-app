export const changeSpace = {
    data() {
        return {
            spaceNo: null
        }
    },
    methods: {
        changeSpace(space) {
            this.spaceNo = space.no;
        },
    }
}