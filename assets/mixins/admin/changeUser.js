export const changeUser = {
    data() {
        return {
            userNo: null
        }
    },
    methods: {
        changeUser(user) {
            this.userNo = user.no;
        },
    }
}