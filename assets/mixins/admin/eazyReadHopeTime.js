export const eazyReadHopeTime = {
    methods: {
        eazyReadHopeTime(hopeTime) {
            switch (hopeTime) {
                case "1012":
                    return "오전 10시 ~ 오전 12시";
                case "1416":
                    return "오후 2시 ~ 오후 4시";
                case "1618":
                    return "오후 4시 ~ 오후 6시";
                default:
                    return "상시 가능";
            }
        }
    }
}