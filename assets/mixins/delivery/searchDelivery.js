import { Subject } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';

export const searchDelivery = {
  data() {
    return {
      searchKeyword: '',
      results: [],
      selectedArr: [],
      keywordLoading: false,
    };
  },
  props: { dropDownEndpoint: String, mapRoute: String },
  subscriptions() {
    this.change$ = new Subject();
    return {
      //입력한 후 0.5초가 지나야 input이 바뀜
      input: this.change$.pipe(
        debounceTime(500),
        distinctUntilChanged(),
        map((event) => (this.searchKeyword = event.event.target.value))
      ),
    };
  },
  watch: {
    searchKeyword() {
      if (this.searchKeyword) {
        this.results = [];
        this.selectedArr = []
        this.keywordLoading = true;
        return this.$axios
          .$get(this.dropDownEndpoint, {
            params: { keyword: this.searchKeyword },
          })
          .then((res) => {
            this.keywordLoading = false;
            //어레이를 합치고 selected property를 넣음
            let tempArr = res.topResults
            let results = tempArr.concat(res.secondResults);
            if (results.length > 0) {
              this.selectedArr = Array(results.length).fill(false);
              this.selectedArr[0] = true;
            }
            this.results = results
          });
      }
    },
  },
  methods: {
    isSelected(item) {
      if (item.selected) {
        if (item.selected === true) {
          return 'selected';
        }
      }
    },
    resetKeyword() {
      this.searchKeyword = '';
      this.$refs.searchInput.value = '';
      this.results = [];
      this.selectedArr = []
    },
    onDropdownClicked(value) {
      this.goToMap(value);
    },
    onSearchBtnClicked() {
      if (this.results.length > 0) {
        //   remove previous selection
        const allMarkers = document.querySelectorAll('div.marker');
        allMarkers.forEach((marker) => {
          marker.classList.remove('is-active');
        });
        const theIndex = this.findSelectedIndex();
        this.goToMap(this.results[theIndex].name);
      }
    },
    onKeyUp() {
      let theIndex = this.findSelectedIndex();
      if (theIndex > 0) {
        this.$set(this.selectedArr, theIndex, false);
        this.$set(this.selectedArr, theIndex - 1, true);
        // this.$refs.searchInput.value = this.results[theIndex].name;     
      } else {
        this.$set(this.selectedArr, theIndex, false);
        this.$set(this.selectedArr, this.selectedArr.length - 1, true);
      }
    },
    onKeyDown() {
      let theIndex = this.findSelectedIndex();
      if (this.results.length > theIndex + 1) {
        this.$set(this.selectedArr, theIndex, false);
        this.$set(this.selectedArr, theIndex + 1, true);
        // this.$refs.searchInput.value = this.results[theIndex].name;      
      } else {
        this.$set(this.selectedArr, theIndex, false);
        this.$set(this.selectedArr, 0, true);
      }
    },
    findSelectedIndex() {
      const isSelected = (e) => e === true;
      const theIndex = this.selectedArr.findIndex(isSelected);
      return theIndex;
    },
    goToMap(value) {
      return this.$axios
        .$get('/companyDistrict/getCenter', {
          params: { keyword: value },
        })
        .then((res) => {
          this.$router.push({
            name: this.mapRoute,
            query: { lat: res.lat, lon: res.lon, searchKeyword: value }
          }
          )
        });
    },
  },
};
