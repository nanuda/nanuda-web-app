export const sendingFormData = {
    data() {
        return {
            form: process.client ? new FormData() : null,
            // form: new FormData()
        }
    },
    methods: {
        appendForm(value, field, name) {
            // console.log("arguments.length", arguments.length);
            if (arguments.length == 2) {
                if (value) {
                    this.form.append(field, value);
                }
            } else {
                if (value) {
                    this.form.append(field, value, name);
                }
            }
        },
    }
}