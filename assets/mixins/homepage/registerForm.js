import AddressSelectBox from '@/components/Utils/AddressSelectBox';
import PrivacyRestaurant from '@/components/Agreement/PrivacyRestaurant';
import { required, minLength, maxLength } from 'vuelidate/lib/validators';

export const registerForm = {
  components: {
    AddressSelectBox,
    PrivacyRestaurant,
  },
  data() {
    return {
      showInformedConsent: false,
      isInformedConsentChecked: false,
      name: '',
      phone: '',
      city: '',
      district: '',
      changupExpYn: '',
      age: '',
      personalMenu: '',
      changupPlanning: '',
      foodCategoryType: '',
      etc: '',
      type: '개인',
    };
  },
  methods: {
    // 약관동의
    isInformedConsent() {
      if (!this.isInformedConsentChecked) {
        this.isInformedConsentChecked = !this.isInformedConsentChecked;
        // this.showInformedConsent = !this.showInformedConsent;
      } else {
        this.isInformedConsentChecked = !this.isInformedConsentChecked;
      }
    },
    onSelectedCity(value) {
      this.city = value;
      const districtValue = false;
      this.onSelectedDistrict(districtValue);
    },
    onSelectedDistrict(value) {
      if (!value) {
        this.district = '';
      }
      if (value) {
        this.district = value;
      }
    },
  },
  validations: {
    type: {
      required,
    },
    name: {
      required,
    },
    phone: {
      required,
    },
    city: {
      required,
    },
  },
};
