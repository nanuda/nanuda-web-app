export const common = {
  methods: {
    moreSpaceView(type) {
      // 공간유형에 따른 리스트 더보기
      this.$store.dispatch('spaces/getSpaces', {
        SPACE_TYPE_NO: type,
      });
      this.$router.push({ name: 'spaces', params: { spaceType: type } });
    },
  },
};
