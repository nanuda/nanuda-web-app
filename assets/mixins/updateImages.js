export const updateImages = {
    date() {
        return {
            images: null
        }
    },
    methods: {
        updateImages(files, category) {
            let arr = []
            files[category].map(e => {
                arr.push(e.file);
            });
            this.images = arr;
        },
    }
}