import AppRequestBox from '@/components/Plans/AppRequestBox';

export const infoAppRequest = {
  components: {
    AppRequestBox,
  },
  data() {
    return {
      isScrolled: false,
    };
  },
  methods: {
    appVisitFixed() {
      // 방문신청 고정 이벤트
      const target = this.$refs.articleContent;
      if (target) {
        let rect =
          target.getBoundingClientRect().top + this.currentScroll ||
          target.offsetTop;
        if (this.currentScroll > rect) {
          this.isScrolled = true;
        } else {
          this.isScrolled = false;
        }
      }
    },
    // spaceTypeNo : 1 식당형, 2: 배달형
    // brandNo : 브랜드 id
    toFormPage(spaceTypeNo, brandNo) {
      this.$router.push({
        name: 'plans-form',
        params: {
          spaceType: spaceTypeNo,
          brandNo: brandNo,
        },
      });
    },
  },
};
