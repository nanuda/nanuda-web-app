export const searchSpaces = {
  data() {
    return {
      selectedSpaceType: '',
      selectedSido: '',
      selectedGungu: '',
    };
  },
  methods: {
    onSelectSpaceType(value) {
      this.selectedSpaceType = value;
    },
    onSelectSido(value) {
      this.selectedSido = value;
      this.selectedGungu = '';
    },
    onSelectGungu(value) {
      if (value === '') {
        this.selectedGungu = this.selectedSido;
      } else {
        this.selectedGungu = value;
      }
    },
  },
  computed: {
    filteredGungu() {
      const gungus = this.addresses.find((e) => e.CODE === this.selectedSido);
      let result = null;
      //gunsgus 가 존재하면 sigungus의 값을 리턴함 없으면 null 리턴
      if (gungus) {
        result = gungus.SIGUNGUS;
      }
      return result;
    },
    queryCode() {
      if (this.selectedGungu) {
        return this.selectedGungu;
      } else {
        this.selectedGungu = '';
        return this.selectedSido;
      }
    },
  },
};
