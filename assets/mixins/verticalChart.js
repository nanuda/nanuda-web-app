export const verticalChart = {
    props: ["dataGroup"],
    methods: {
        //ex)12.3같은 값을가진 어레이를 만듬
        dataArr() {
            let tempArr = Object.values(this.dataGroup);
            const intArr = tempArr.map(e => {
                return Number.parseFloat((e * 100).toFixed(1));
            });
            return intArr;
        },
        //1,2등을 주황색 나머지를 회색으로 하는 데이터길이만큼의 어레이를 만듬
        backgroundColor() {
            let ageArr = this.dataArr();
            const fullLength = ageArr.length;
            const firstIndex = ageArr.indexOf(Math.max(...ageArr));
            const secondIndex = ageArr.indexOf(this.secondMax(ageArr));
            const colorArr = Array(fullLength).fill("#d5d5d5");
            colorArr.splice(firstIndex, 1, "#ff7a00");
            colorArr.splice(secondIndex, 1, "#ff7a00");
            return colorArr;
        },
        secondMax(arr) {
            var max = Math.max.apply(null, arr), // get the max of the array
                maxi = arr.indexOf(max);
            arr[maxi] = -Infinity; // replace max in the array with -infinity
            var secondMax = Math.max.apply(null, arr); // get the new max 
            arr[maxi] = max;
            return secondMax;
        }
    }
}