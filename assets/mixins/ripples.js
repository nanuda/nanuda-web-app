export const ripples = {
  // 버튼 및 요소 mousde down 시 해당 커서 위치로부터 원 ripple 애니메이션 효과
  props: {
    color: {
      type: String,
      default: 'rgba(255, 255, 255, 0.5)',
    },
    bgColor: {
      type: String,
      default: 'rgba(255, 255, 255, 0.5)',
    },
  },
  data() {
    return {
      ripples: [],
    };
  },
  mounted() {
    const width = this.$refs.container.offsetWidth;
    const height = this.$refs.container.offsetHeight;
    this.rippleWidth = width > height ? width : height;
    this.halfRippleWidth = this.rippleWidth / 2;

    window.addEventListener('mouseup', this.purgeRipples);
  },
  beforeDestroy() {
    window.removeEventListener('mouseup', this.purgeRipples);
  },
  methods: {
    click() {
      this.$emit('click');
    },
    addRipple(e) {
      const { left, top } = this.$refs.container.getBoundingClientRect();
      const rippleId = Date.now();
      this.ripples.push({
        width: `${this.rippleWidth}px`,
        height: `${this.rippleWidth}px`,
        left: `${e.clientX - left - this.halfRippleWidth}px`,
        top: `${e.clientY - top - this.halfRippleWidth}px`,
        id: rippleId,
      });
    },
    purgeRipples() {
      this.ripples = [];
    },
  },
};
