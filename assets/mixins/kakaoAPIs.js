import { testMixin } from './testMixin';
import Vue from 'vue';

// const kakao = new kakao();
export const kakaoAPIs = {
  components: {},
  data() {
    return {
      map: null,
      clusterer: null,
      marker: null,
      coords: {
        lat: null,
        lng: null,
      },
      // mapTypeId: VueDaumMap.MapTypeId.NORMAL, // 맵 타입

      showPostcode: false,
      libraries: ['services', 'clusterer'], // 추가로 불러올 라이브러리
      // mapTypeId: VueDaumMap.MapTypeId.NORMAL, // 맵 타입
      appKey: 'ec5ed892308b6e2ac7add9a9ca2ee159', // Nate appkey
      level: 7, // 지도의 레벨(확대, 축소 정도),
      zonecode: null,
      address: null,
      addressEnglish: null,
      addressType: null,
      bcode: null,
      bname: null,
      bname1: null,
      bname2: null,
      sido: null,
      sigungu: null,
      sigunguCode: null,
      userLanguageType: null,
      query: null,
      buildingName: null,
      buildingCode: null,
      apartment: null,
      jibunAddress: null,
      jibunAddressEnglish: null,
      roadAddress: null,
      roadAddressEnglish: null,
      autoRoadAddress: null,
      autoRoadAddressEnglish: null,
      autoJibunAddress: null,
      autoJibunAddressEnglish: null,
      userSelectedType: null,
      noSelected: null,
      hname: null,
      roadnameCode: null,
      roadname: null,
      bottomSpaceLoading: false,
      likeLoading: false,
      countLoading: false,
      isCollapsed: true,
      selectedMarker: null,
      transparentIcon: null,
      isKitchenApplianceTapOpen: false,
      isSearchLayerVisible: false,
      isFilterLayerVisible: false,
      experience: { businessLocation: null }
    };
  },
  computed: {
    bottomCardLoading() {
      if (this.bottomSpaceLoading || this.likeLoading || this.countLoading) {
        return true;
      } else {
        return false;
      }
    },
  },
  methods: {
    onAdminLoad(map) {
      this.map = map;
      //마커를 생성후 data에 할당 setAddress에 필요
      const marker = new kakao.maps.Marker({
        position: new kakao.maps.LatLng(this.coords.lat, this.coords.lng),
        level: 3,
      });
      marker.setMap(map);
    },
    onSearchLayerOpen() {
      this.isSearchLayerVisible = !this.isSearchLayerVisible;
      this.isFilterLayerVisible = false;
    },
    onFilterLayerOpen() {
      this.isFilterLayerVisible = !this.isFilterLayerVisible;
      this.isSearchLayerVisible = false;
    },
    onLoad(map) {
      if (this.minLevel) {
        map.setMinLevel(this.minLevel);
      }
      this.map = map;
      const overlay = new kakao.maps.CustomOverlay({
        position: new kakao.maps.LatLng(this.coords.lat, this.coords.lng),
        clickable: false,
        content: `<div class="marker"><span class="count"></span></div>`,
        zIndex: 1,
      });
      overlay.setMap(this.map);
    },
    onDeliveryClustererLoad(map) {
      let selectedMarker = null;
      if (this.minLevel) {
        map.setMinLevel(this.minLevel);
      }
      this.map = map;
      this.clusterer = new kakao.maps.MarkerClusterer({
        map: map, // 마커들을 클러스터로 관리하고 표시할 지도 객체
        averageCenter: true, // 클러스터에 포함된 마커들의 평균 위치를 클러스터 마커 위치로 설정
        minLevel: 9, // 클러스터 할 최소 지도 레벨
        maxLevel: 15,
        texts: getTexts,
        styles: [
          {
            // calculator 각 사이 값 마다 적용될 스타일을 지정한다
            padding: '7px 10px',
            background: '#635dff',
            borderRadius: '25px',
            color: '#fff',
            fontSize: '18px',
            textAlign: 'center',
          },
        ],
      });
      //   customize text field for cluste
      function getTexts(count) {
        return `<span style="font-size: 12px">지점 총 수</span> <br> <b> ${Math.floor(
          count / 2,
        )}</b>개`;
      }
      const resetSelection = () => {
        this.changeCollapse(true).then(() => {
          // 맵 초기화
          if (selectedMarker) {
            selectedMarker.setImage(whiteIcon);
            selectedMarker = null;
            const allMarkers = document.querySelectorAll('div.marker');
            allMarkers.forEach((marker) => {
              marker.classList.remove('is-active');
            });
          }
          this.bottomSpaceList = [];
          this.bottomSpaceListCount = 0;
          this.isSpaceListfixed = false;
          this.selectedDistrictNo = null;
        });
      };
      // //   zoomable control
      // const zoomControl = new kakao.maps.ZoomControl();
      // map.addControl(zoomControl, kakao.maps.ControlPosition.RIGHT);
      // //   sky view control
      // const mapTypeControl = new kakao.maps.MapTypeControl();
      // map.addControl(mapTypeControl, kakao.maps.ControlPosition.TOPRIGHT);

      //   get custom image
      const icon = new kakao.maps.MarkerImage(
        'https://kr.object.ncloudstorage.com/common-nanuda/icon_marker_transparent.png',
        new kakao.maps.Size(39, 50),
      );
      const whiteIcon = new kakao.maps.MarkerImage(
        'https://kr.object.ncloudstorage.com/common-nanuda/icon_marker_transparent.png',
        new kakao.maps.Size(39, 50),
      );
      // over hundred
      const overHundred = (count) => {
        if (count > 99) {
          return '99+';
        } else {
          return count;
        }
      };
      const overlays = this.deliverySpaces.map((deliverySpace) => {
        let wholeQuantity = 0;
        deliverySpace.deliverySpaces.map((e) => {
          wholeQuantity += e.quantity - e.contracts.length;
        });
        const overlay = new kakao.maps.CustomOverlay({
          position: new kakao.maps.LatLng(deliverySpace.lat, deliverySpace.lon),
          clickable: false,
          content: `<div id=${deliverySpace.no
            } class="marker"><span class="count">${overHundred(
              wholeQuantity,
            )}</span></div>`,
          zIndex: 1,
        });
        if (this.selectedDistrictNo == deliverySpace.no) {
          const overlay = new kakao.maps.CustomOverlay({
            position: new kakao.maps.LatLng(
              deliverySpace.lat,
              deliverySpace.lon,
            ),
            clickable: false,
            content: `<div id=${deliverySpace.no
              } class="marker is-active"><span class="count">${overHundred(
                wholeQuantity,
              )}</span></div>`,
            zIndex: 1,
          });
          // const moveLatLon = new kakao.maps.LatLng(
          //   deliverySpace.lat,
          //   deliverySpace.lon,
          // );
          // map.panTo(moveLatLon);
          return overlay;
        }
        return overlay;
      });
      const markers = this.deliverySpaces.map((deliverySpace) => {
        const marker = new kakao.maps.Marker({
          position: new kakao.maps.LatLng(deliverySpace.lat, deliverySpace.lon),
          clickable: true,
          image: whiteIcon,
          zIndex: 2,
        });
        kakao.maps.event.addListener(marker, 'click', () => {
          this.changeCollapse(false).then(() => {
            map.relayout();
            this.bottomSpaceList = [];
            this.bottomSpaceLoading = true;
            const chosenMarker = sessionStorage.getItem('chosen_marker');

            const previousMarker = document.getElementById(chosenMarker);
            if (previousMarker) {
              previousMarker.classList.remove('is-active');
            }
            sessionStorage.setItem('chosen_marker', deliverySpace.no);
            const selectMarker = document.getElementById(deliverySpace.no);
            if (selectMarker) {
              selectMarker.classList.add('is-active');
            }

            this.panToCenter(map, deliverySpace);
            // 현재 클릭된 마커의 이미지는 클릭 이미지로 변경합니다
            marker.setImage(icon);
            selectedMarker = marker;
            this.selectedDistrictNo = deliverySpace.no;
            this.setQuerys();
            map.relayout();
            this.$axios
              .$get('/deliverySpace', {
                params: {
                  companyDistrictNo: deliverySpace.no,
                  nanudaUserNo: this.$store.state.userNum,
                },
              })
              .then((res) => {
                this.bottomSpaceLoading = false;
                this.bottomSpaceList = res.items;
                this.bottomSpaceListCount = res.totalCount;
              });
          });
        });
        //url에 선택된 공간no가 있다면 클릭 트리거
        if (this.selectedDistrictNo == deliverySpace.no) {
          kakao.maps.event.trigger(marker, 'click');
        }
        return marker;
      });
      // kakao.maps.event.addListener(map, 'click', makeOverListener);
      // function makeOverListener() {
      //     console.log(123);
      // }
      // kakao.maps.event.addListener(map, 'idle', () => {
      //   map.relayout()
      // });
      //마커, 타입리스트 초기화
      kakao.maps.event.addListener(map, 'idle', () => {
        this.$nextTick(() => {
          map.relayout();
        });
      });
      //마커, 타입리스트 초기화
      kakao.maps.event.addListener(map, 'dragstart', () => {
        resetSelection();
        this.$nextTick(() => {
          map.relayout();
        });
        this.setQuerys();
      });
      kakao.maps.event.addListener(map, 'click', () => {
        resetSelection();
        this.$nextTick(() => {
          map.relayout();
        });
        this.setQuerys();
      });
      kakao.maps.event.addListener(map, 'zoom_start', () => {
        resetSelection();
        this.setQuerys();
      });
      this.clusterer.addMarkers(overlays);
      this.clusterer.addMarkers(markers);
    },
    onRestaurantClustererLoad(map) {
      let selectedMarker = null;
      if (this.minLevel) {
        map.setMinLevel(this.minLevel);
      }
      this.map = map;
      this.clusterer = new kakao.maps.MarkerClusterer({
        map: map, // 마커들을 클러스터로 관리하고 표시할 지도 객체
        averageCenter: true, // 클러스터에 포함된 마커들의 평균 위치를 클러스터 마커 위치로 설정
        minLevel: 9, // 클러스터 할 최소 지도 레벨
        maxLevel: 15,
        texts: getTexts,
        styles: [
          {
            // calculator 각 사이 값 마다 적용될 스타일을 지정한다
            padding: '10px',
            minWidth: '50px',
            background: '#635dff',
            borderRadius: '50%',
            color: '#fff',
            fontSize: '25px',
            textAlign: 'center',
          },
        ],
      });
      //   customize text field for cluste
      function getTexts(count) {
        return `<span>${Math.floor(count / 2)}</span>`;
      }
      const resetSelection = () => {
        this.changeCollapse(true).then(() => {
          // 맵 초기화
          if (selectedMarker) {
            selectedMarker.setImage(whiteIcon);
            selectedMarker = null;
            const allMarkers = document.querySelectorAll('div.marker');
            allMarkers.forEach((marker) => {
              marker.classList.remove('is-active');
            });
          }
          this.selectedSpaceNo = null;
          this.bottomSpace = null;
          this.isSpaceListfixed = false;
        });
      };
      // //   zoomable control
      // const zoomControl = new kakao.maps.ZoomControl();
      // map.addControl(zoomControl, kakao.maps.ControlPosition.RIGHT);
      // //   sky view control
      // const mapTypeControl = new kakao.maps.MapTypeControl();
      // map.addControl(mapTypeControl, kakao.maps.ControlPosition.TOPRIGHT);

      //   get custom image
      const icon = new kakao.maps.MarkerImage(
        'https://kr.object.ncloudstorage.com/common-nanuda/icon_marker_transparent.png',
        new kakao.maps.Size(39, 50),
      );
      const whiteIcon = new kakao.maps.MarkerImage(
        'https://kr.object.ncloudstorage.com/common-nanuda/icon_marker_transparent.png',
        new kakao.maps.Size(39, 50),
      );
      const overlays = this.restaurantSpaces.map((restaurantSpace) => {
        const overlay = new kakao.maps.CustomOverlay({
          position: new kakao.maps.LatLng(
            restaurantSpace.lat,
            restaurantSpace.lon,
          ),
          clickable: false,
          content: `<div id=${restaurantSpace.no} class="marker"><span class="count"></span></div>`,
          zIndex: 1,
        });
        if (this.selectedSpaceNo == restaurantSpace.no) {
          const overlay = new kakao.maps.CustomOverlay({
            position: new kakao.maps.LatLng(
              restaurantSpace.lat,
              restaurantSpace.lon,
            ),
            clickable: false,
            content: `<div id=${restaurantSpace.no} class="marker is-active"><span class="count"></span></div>`,
            zIndex: 1,
          });
          // const moveLatLon = new kakao.maps.LatLng(
          //   restaurantSpace.lat,
          //   restaurantSpace.lon,
          // );
          // map.panTo(moveLatLon);
          return overlay;
        }
        return overlay;
      });
      const markers = this.restaurantSpaces.map((restaurantSpace) => {
        const marker = new kakao.maps.Marker({
          position: new kakao.maps.LatLng(
            restaurantSpace.lat,
            restaurantSpace.lon,
          ),
          clickable: true,
          image: whiteIcon,
          zIndex: 2,
        });
        kakao.maps.event.addListener(marker, 'click', () => {
          this.changeCollapse(false).then(() => {
            map.relayout();
            this.bottomSpace = null;
            const chosenMarker = sessionStorage.getItem('chosen_marker');
            const previousMarker = document.getElementById(chosenMarker);
            if (previousMarker) {
              previousMarker.classList.remove('is-active');
            }
            sessionStorage.setItem('chosen_marker', restaurantSpace.no);
            const selectMarker = document.getElementById(restaurantSpace.no);
            if (selectMarker) {
              selectMarker.classList.add('is-active');
            }
            // 현재 클릭된 마커의 이미지는 클릭 이미지로 변경합니다
            marker.setImage(icon);

            selectedMarker = marker;
            this.bottomSpaceLoading = true;
            this.$axios.$get(`/space/${restaurantSpace.no}`).then((res) => {
              this.bottomSpace = res;
              this.bottomSpaceLoading = false;
            });
            if (this.$store.getters.isAuthenticated) {
              this.likeLoading = true;
              this.$axios
                .$get(
                  `/favoriteSpace/checkForRestaurantKitchen/${restaurantSpace.no}`,
                )
                .then((res) => {
                  this.likeLoading = false;
                  this.isSpaceLiked = res.isLiked === 'Y' ? true : false;
                });
            }
            this.countLoading = true;
            this.$axios
              .$get(
                `/nonUserFavorite/checkCountForRestaurantKitchen/${restaurantSpace.no}`,
              )
              .then((res) => {
                this.countLoading = false;
                this.spaceLikedCount = res;
              });
            // const moveLatLon = new kakao.maps.LatLng(
            //   restaurantSpace.lat,
            //   restaurantSpace.lon,
            // );
            // console.log('moveLatLon', moveLatLon)
            // map.panTo(moveLatLon);
            this.panToCenter(map, restaurantSpace);
            this.selectedSpaceNo = restaurantSpace.no;
            this.setQuerys();
            map.relayout();
          });
        });
        //// 클릭 이벤트 리스너

        // url에 선택된 공간no가 있다면 클릭 트리거
        if (this.selectedSpaceNo == restaurantSpace.no) {
          kakao.maps.event.trigger(marker, 'click');
        }
        return marker;
      });
      kakao.maps.event.addListener(map, 'idle', () => {
        this.$nextTick(() => {
          map.relayout();
        });
      });
      //마커, 타입리스트 초기화
      kakao.maps.event.addListener(map, 'dragstart', () => {
        resetSelection();
        this.$nextTick(() => {
          map.relayout();
        });
        this.setQuerys();
      });
      kakao.maps.event.addListener(map, 'click', () => {
        resetSelection();
        this.$nextTick(() => {
          map.relayout();
        });
        this.setQuerys();
      });
      kakao.maps.event.addListener(map, 'zoom_start', () => {
        resetSelection();
        this.setQuerys();
      });
      this.clusterer.addMarkers(overlays);
      this.clusterer.addMarkers(markers);
    },
    setQuerys() {
      this.$router.replace({ query: this.querys }).catch((err) => { });
    },
    changeCollapse(value) {
      return new Promise((resolve, reject) => {
        this.isCollapsed = value;
        resolve();
      });
    },
    panToCenter(map, space) {
      const moveLatLon = new kakao.maps.LatLng(space.lat, space.lon);
      map.panTo(moveLatLon);
    },
    goToLogin(res) {
      if (res.ok) {
        this.$router.push('/login');
      } else {
        this.isVisible = false;
      }
    },
    setAddress(res, ref) {
      return new Promise((resolve) => {
        this.zonecode = res.zonecode;
        this.address = res.address;
        this.experience.businessLocation = res.address;
        this.addressEnglish = res.addressEnglish;
        this.addressType = res.addressType;
        this.bcode = res.bcode;
        this.bname = res.bname;
        this.bname1 = res.bname1;
        this.bname2 = res.bname2;
        this.sido = res.sido;
        this.sigungu = res.sigungu;
        this.sigunguCode = res.sigunguCode;
        this.userLanguageTyp = res.userLanguageTyp;
        this.query = res.query;
        this.buildingName = res.buildingName;
        this.buildingCode = res.buildingCode;
        this.apartment = res.apartment;
        this.jibunAddress = res.jibunAddress;
        this.jibunAddressEnglish = res.jibunAddressEnglish;
        this.roadAddress = res.roadAddress;
        this.roadAddressEnglish = res.roadAddressEnglish;
        this.autoRoadAddress = res.autoRoadAddress;
        this.autoRoadAddressEnglish = res.autoRoadAddressEnglish;
        this.autoJibunAddress = res.autoJibunAddress;
        this.autoJibunAddressEnglish = res.autoJibunAddressEnglish;
        this.userSelectedType = res.userSelectedType;
        this.noSelected = res.noSelected;
        this.hname = res.hname;
        this.roadnameCode = res.roadnameCode;
        this.roadname = res.roadname;
        const geocoder = new kakao.maps.services.Geocoder();

        const callback = (results, status) => {
          if (status === kakao.maps.services.Status.OK) {
            const result = results[0]; //첫번째 결과의 값을 활용 // 해당 주소에 대한 좌표를 받아서
            this.coords.lng = result.x;
            this.coords.lat = result.y;
            resolve('resolved');
          }
        };
        //주소로 좌표를 가져옴
        geocoder.addressSearch(res.address, callback);
        this.showPostcode = !this.showPostcode;
      });
    },
  },
};
