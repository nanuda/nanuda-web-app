export const validations = {
    methods: {
        required: value => !!value || "필수항목",
        int: v => /^[-+]?[1-9]\d*$/.test(v) || '정수만 가능',
        number: v => /^[0-9]+$/.test(v) || '숫자만 가능',
        length(value, length) {
            return value.length == length || length + '자리만 가능'
        },
        max(value, length) {
            return value.length <= length || length + '자리보다 작아야합니다'
        },
        min(value, length) {
            return value.length >= length || length + '자리보다 커야합니다'
        },
        noSpace: v => /^\S*$/.test(v) || '띄어쓰기 불가능',
        fullHangul(v) {
            const hangul = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/
            const notFullHangul = /[ㄱ-ㅎ|ㅏ-ㅣ]/
            if (hangul.test(v)) {
                if (notFullHangul.test(v)) {
                    return '완전한 한글이어야합니다.'
                }
            }
            return true
        }
    }
}