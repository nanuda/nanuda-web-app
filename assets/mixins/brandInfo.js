import BrandModal from '@/components/Brands/BrandModal';
import BrandList from '@/components/Brands/BrandList';

export const brandInfo = {
  components: {
    BrandModal,
    BrandList,
  },
  data() {
    return {
      baseUrl: process.env.baseUrl,
      showBrandModal: false, // 모달 visible 여부
      brandInfo: null,
      menuInfo: null,
    };
  },
  methods: {
    showBrandInfo(no) {
      // 브랜드 상세 보기
      return this.$axios
        .$get(`/brand/${no}`)
        .then((res) => {
          this.showBrandModal = true;
          this.brandInfo = res;
          this.menuInfo = res.MENU;
        })
        .catch((e) => {
          console.log(e);
        });
    },
    closeBrandModal() {
      this.showBrandModal = false;
    },
  },
};
