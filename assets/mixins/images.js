export const images = {
  data() {
    return {
      images: null
    };
  },
  methods: {
    //이미지 삭제 메소드
    deleteImage(no) {
      this.$axios
        .$delete(`/fileUpload/singleDestroy/${no}`)
        .then(res => {
          //삭제하려는 파일 인덱스 찾기
          const theIndex = this.images.findIndex(image => {
            return image.NO == no;
          });
          //삭제
          this.images.splice(theIndex, 1);
        })
        .catch(e => {
          console.log("error", e);
        });
    },
    //업로드가 성공하면 호출되는 메소드
    //데이터베이스와 로컬의 싱크를 맞춤
    onUpdateImages(updateImages) {
      this.images = this.images.concat(updateImages);
    }
  }
};
