export const handleScroll = {
    data() {
        return {
            lastScroll: 0, // 마지막 스크롤
            currentScroll: 0, // 현재 스크롤
            winHeight: 0, // 윈도우 높이
        }
    },
    beforeDestroy() {
        window.removeEventListener("scroll", this.onScroll);
    },
    mounted() {
        window.addEventListener("scroll", this.onScroll);
    },
    methods: {
        handleScroll() {
            const body = document.body;
            const scrollUp = "up";
            const scrollDown = "down";


            this.currentScroll = window.pageYOffset || window.scrollY;

            if (this.currentScroll == 0) {
                body.classList.remove(scrollUp);
                return;
            }

            if (
                this.currentScroll > this.lastScroll &&
                !body.classList.contains(scrollDown)
            ) {
                // down
                body.classList.remove(scrollUp);
                body.classList.add(scrollDown);
            } else if (
                this.currentScroll < this.lastScroll &&
                body.classList.contains(scrollDown)
            ) {
                // up
                body.classList.remove(scrollDown);
                body.classList.add(scrollUp);
            }
            this.lastScroll = this.currentScroll;
            this.winHeight = window.outerHeight;


        },
        onScroll() {
            this.handleScroll();
            this.scrollFunc();
        },
        scrollFunc() {
            // 스크롤 관련 이벤트 
        }
    },
}