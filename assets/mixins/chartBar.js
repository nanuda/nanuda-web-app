import { Bar, mixins } from 'vue-chartjs';
import ChartJsPluginDataLabels from 'chartjs-plugin-datalabels';
const { reactiveProp } = mixins;

export const chartBar = {
  extends: Bar,
  mixins: [reactiveProp],
  props: ['chartData'],
  components: {
    ChartJsPluginDataLabels,
  },
  data() {
    return {
      data: {
        //barPercentage,  categoryPercentage 속성값 datasets 에서 설정
        datasets: [
          {
            barPercentage: 0.6,
            categoryPercentage: 1,
          },
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: true,
        legend: {
          display: false,
        },
        showTooltips: true,
        tooltips: {
          // 툴팁 % 단위 표시 추가
          callbacks: {
            label: function (tooltipItem) {
              return `${tooltipItem.yLabel}%`;
            },
          },
        },
        scales: {
          xAxes: [
            {
              gridLines: {
                display: false,
              },
              ticks: {
                beginAtZero: true,
              },
            },
          ],
          yAxes: [
            {
              display: false,
              ticks: {
                beginAtZero: true,
                stepSize: 20,
              },
              gridLines: {
                display: false,
              },
            },
          ],
        },
        plugins: {
          datalabels: {
            align: 'bottom',
            anchor: 'end',
            borderRadius: 4,
            color: 'white',
            // backgroundColor: function(context) {
            //   return context.dataset.backgroundColor;
            // },
            formatter: function (value, context) {
              const arr = context.dataset.data;
              const sorted = arr.slice().sort(function (a, b) {
                return b - a;
              });
              const ranks = arr.slice().map(function (v) {
                return sorted.indexOf(v) + 1;
              });

              // context.chart.data.labels[context.dataIndex];
              return ranks[context.dataIndex];
            },
            display: function (context) {
              const arr = context.dataset.data;
              const sorted = arr.slice().sort(function (a, b) {
                return b - a;
              });
              const ranks = arr.slice().map(function (v) {
                return sorted.indexOf(v) + 1;
              });

              return ranks[context.dataIndex] < 3;
            },
          },
        },
      },
    };
  },
  mounted() {
    this.renderChart(this.data, this.options);
  },
};
