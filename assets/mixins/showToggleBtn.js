export const showToggleBtn = {
    data() {
        return {
            isShowing: "N"
        }
    },
    methods: {
        toggleShow(ref) {
            this.isShowing == "Y" ? (this.isShowing = "N") : (this.isShowing = "Y");
        }
    }
}