
export default {
    msgSize(msg) {
        let size = 0;
        let korArrleng = 0;
        let otherCharlength = 0;
        let koreanArr = [];
        const koreanRegex = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g;

        //메세지 내용중에서 한글만으로 어레이를 만듬
        koreanArr = msg.match(koreanRegex);
        // console.log(koreanArr);
        if (koreanArr) {
            korArrleng = koreanArr.length;
        }
        // console.log("korArrleng: " + korArrleng);
        //메세지길이에서 한글길이만 뺌
        otherCharlength = msg.length - korArrleng;
        // console.log("otherCharlength: " + otherCharlength);
        //바이트사이즈 = 한글을 제외한 모든 char + 한글*2
        size = otherCharlength + korArrleng * 2;
        // console.log("size: "+ size)

        return size;
    }
}
