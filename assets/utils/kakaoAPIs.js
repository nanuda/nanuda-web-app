export default {
    onLoad(map, coords, thisMap, thisMarker) {
        // 마커를 생성후 data에 할당     
        this['map'] = map;
        thisMarker = new kakao.maps.Marker({
            position: new kakao.maps.LatLng(coords.lat, coords.lng),
            // position: map.getCenter(),
            map: thisMap
        });

    },
    setAddress(res, map, marker, coords, thisAddr) {
        thisAddr = res.address;
        const geocoder = new kakao.maps.services.Geocoder();
        const callback = (results, status) => {
            if (status === kakao.maps.services.Status.OK) {
                const result = results[0]; //첫번째 결과의 값을 활용 // 해당 주소에 대한 좌표를 받아서
                const coords = new kakao.maps.LatLng(result.y, result.x); // 지도를 보여준다.            
                // mapContainer.style.display = "block";
                map.relayout(); // 지도 중심을 변경한다.
                map.setCenter(coords); // 마커를 결과값으로 받은 위치로 옮긴다.
                marker.setPosition(coords);
                // this.changeInfo(ref, 'ADDR_DEP1');
            }
        };
        //주소로 좌표를 가져옴
        geocoder.addressSearch(res.address, callback);
        // this.showPostcode = !this.showPostcode;
    },
}